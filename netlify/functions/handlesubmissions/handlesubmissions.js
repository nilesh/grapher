// Docs on event and context https://docs.netlify.com/functions/build/#code-your-function-2
const knownTrees = {
	nanotech: { recipient: process.env.DEFAULT_RECIPIENT, password: process.env.DEFAULT_TREE_SECRET },
	neurotech: { recipient: process.env.DEFAULT_RECIPIENT, password: process.env.DEFAULT_TREE_SECRET },
	spacetree: { recipient: process.env.DEFAULT_RECIPIENT, password: process.env.DEFAULT_TREE_SECRET },
	longevity: { recipient: process.env.DEFAULT_RECIPIENT, password: process.env.DEFAULT_TREE_SECRET },
	intcoop: { recipient: process.env.DEFAULT_RECIPIENT, password: process.env.DEFAULT_TREE_SECRET },
	example: { recipient: process.env.DEFAULT_RECIPIENT, password: process.env.DEFAULT_TREE_SECRET }
};

const Mailjet = require('node-mailjet');

const mailjet = new Mailjet({
	apiKey: process.env.MAILJET_APIKEY_PUBLIC,
	apiSecret: process.env.MAILJET_APIKEY_PRIVATE
});

const corsHeaders = {
	'Access-Control-Allow-Origin': '*',
	'Access-Control-Allow-Credentials': true,
	'Access-Control-Allow-Headers': 'Content-Type',
	'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'
  };

module.exports.handler = async (event, context) => {
	// console.log({ event });

	if (event.httpMethod !== 'POST') {
		return {
			statusCode: 200,
			corsHeaders,
			body: `Expecting POST request with JSON body`,
		}
	}

	const sender = event.queryStringParameters.sender;
	const treename = event.queryStringParameters.treename;
	const treesecret = event.queryStringParameters.treesecret;

	if (!treename || !knownTrees[treename]) {
		return {
			statusCode: 400,
			body: `Unrecognized treename: ${treename}`,
		}
	}

	if (knownTrees[treename].password !== treesecret) {
		return {
			statusCode: 400,
			body: `Invalid treesecret ${treesecret} for ${treename} tree`,
		}
	}

	console.log(knownTrees, treename);
	const recipient = knownTrees[treename].recipient;
	const body = JSON.parse(event.body);
	const message = body.message;
	const graph = body.graph;
	let regex = new RegExp('[^\\.\\s@:](?:[^\\s@:]*[^\\s@:\\.])?@[^\\.\\s@]+(?:\\.[^\\.\\s@]+)*');

	if (!sender || !message || !sender.trim() || !message.trim() || !regex.test(sender)) {
		return {
			statusCode: 400,
			body: `Invalid sender: ${sender} or message: ${message}`,
		}
	}

	if (!graph) {
		return {
			statusCode: 400,
			body: `Graph missing`,
		}
	}

	console.log(`sending email: to ${recipient}`);

	await mailjet
		.post('send', { version: 'v3.1' })
		.request({
			Messages: [
				{
					From: {
						Email: "no-reply@grapher.polyglot.network",
						Name: "Grapher"
					},
					To: [
						{
							Email: recipient
						}
					],
					Subject: `Changes suggested to ${treename} tree by ${sender}`,
					TextPart: `Someone claiming to be ${sender} has suggested changes for ${treename} tree with this comment:\n\n${message}`,
					Attachments: [
						{
							ContentType: "text/plain",
							Filename: `${treename}.graph`,
							Base64Content: btoa(unescape(encodeURIComponent(JSON.stringify(graph, null, 2))))
						}
					]
				}
			]
		});

	return {
		statusCode: 200,
		body: `Submission handled for ${treename}: ${sender} ${message} ${recipient}`,
	}
}