# Grapher

<img alt="image" src="https://codeberg.org/nilesh/grapher/attachments/673f3846-44de-40a8-b4ab-33646ed27516">

This is an SVG-based interactive graph dataset viewer/editor that allows you to manually lay out nodes and edges but at the same time produce a clean graph-like JSON that can be consumed easily with libraries like Cytoscape JS. You can add arbitrary data fields at nodes and edges. 

The editor mode supports live real-time collaboration so everyone can see what others are working on.

Graphs can be saved in a `.graph` file which stores the array of nodes and edges and some other metadata in JSON format. To see an example of page where this .graph gets pre-loaded, see `index.html`. 

## QuickStart

```bash
npm install
npm run dev
```


## Features

- Live real-time collaboration
- Infinite panning and zooming
- Loading from and saving to a clean JSON-based file format to easily process the graph in your applications.
- Exporting to Cytoscape format or SVG.
- Themability with 4 pre-defined themes.
- Edges can be straight, curved or orthogonal.
- Node description can be markdown including links.
- Custom JSON data fields can be assigned at each node and edge.
- Nodes can be given badges and user can rotate through them by clicking.
- Node can be highlighted by tags.
- Collapsible parent nodes in zommed-out mode
- Select and move multiple nodes at once
- Snap-to-grid for better alignment.

## Built with

- [SvelteJS](https://svelte.dev/) for reactivity
- [Shoelace.Style](https://shoelace.style/) for misc WebComponents

## Related Tools

- [PRSM](https://prsm.uk/)
- [JointJS](https://resources.jointjs.com/demos/kitchensink)