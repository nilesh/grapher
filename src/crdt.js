import { syncedStore, getYjsDoc } from "@syncedstore/core";
import { WebrtcProvider } from "y-webrtc";
import { svelteSyncedStore } from "@syncedstore/svelte";

export const setupSync = (roomName) => {
	// Create your SyncedStore store
	const store = syncedStore({ users: {} });
	
	const svelteStore = svelteSyncedStore(store);
	
	// Create a document that syncs automatically using Y-WebRTC
	const doc = getYjsDoc(store);
	const webrtcProvider = new WebrtcProvider(roomName, doc);

	const disconnect = () => webrtcProvider.disconnect();
	const connect = () => webrtcProvider.connect();

	return svelteStore;
}